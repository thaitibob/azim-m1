package fr.ul.miage.graphique;

import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import fr.ul.miage.baignoire.Baignoire;
import fr.ul.miage.baignoire.Robinet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class progressController implements Initializable{
	
	private Baignoire baignoire = new Baignoire();
	private Robinet robinet = new Robinet();
	private static boolean en_cours = true;
	
	@FXML
	private Button startButton, stopButton, reinitialiser;
	
	@FXML
	private TextField timer, volume_baignoire, debit_fuite, debit_ajout, test;
	
	@FXML
	public TextArea textConsoleDebit, textConsoleFuite;
	
	@FXML
	private ProgressBar progressBar;
	
	@FXML
	void ProgressionDonnees(ActionEvent e) {
		
		Baignoire.fuite = Integer.parseInt(debit_fuite.getText());
		Robinet.debit_entree = Integer.parseInt(debit_ajout.getText());
		Baignoire.volume_max = Integer.parseInt(volume_baignoire.getText());
		
		setValeureBaignoire(debit_fuite.getText(), debit_fuite.getText(), volume_baignoire.getText());
		
		progressController.en_cours = true;
		Instant top = Instant.now();
		ScheduledExecutorService pool = Executors.newScheduledThreadPool(2);
		
		final Runnable arret = new Runnable() {
            @Override
            public void run() {
                while (Baignoire.quantiteEau<Baignoire.volume_max && progressController.en_cours){
                    try {
                        Thread.sleep(50);
                        
                        progressBar.setProgress(Baignoire.quantiteEau/Baignoire.volume_max);    
                        double q = (double) Baignoire.quantiteEau/Baignoire.volume_max;
                        progressBar.setProgress(q);
                        test.setText(String.valueOf(q));
                        Duration duration = Duration.between(top, Instant.now());
                        timer.setText(String.valueOf(duration.getSeconds()));
                        
                    } catch (InterruptedException e) {
                    	System.out.println(e);
                    }
                }
                pool.shutdown();
            }
        };
        
        pool.execute(arret);
        
        pool.scheduleWithFixedDelay(baignoire, 0,100, TimeUnit.MILLISECONDS);
        
        pool.scheduleWithFixedDelay(robinet, 0,100, TimeUnit.MILLISECONDS);
        
        pool.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                String consoleString = textConsoleFuite.getText();
                textConsoleFuite.setText("Quantité Fuité : "+Baignoire.total_fuite+"cm3\n"+consoleString);
            }
        }, 0,1, TimeUnit.SECONDS);
        
        pool.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                String consoleString = textConsoleDebit.getText();
                textConsoleDebit.setText("Quantité ajouté : "+Robinet.total_remplissage+"cm3\n"+consoleString);
            }
        }, 0,1, TimeUnit.SECONDS);
	}
	
	@FXML
	void ArretProgramme(ActionEvent e) {
		progressController.en_cours = false;
	}
	
	@FXML
	void reinitialiserBaignoire(ActionEvent e) {
		progressController.en_cours = false;
		progressBar.setProgress(0.00);
		Baignoire.quantiteEau=0;
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		progressBar.setProgress(0.00);
		
		volume_baignoire.textProperty().addListener(new controleSaisie(volume_baignoire));
		debit_fuite.textProperty().addListener(new controleSaisie(debit_fuite));
		debit_ajout.textProperty().addListener(new controleSaisie(debit_ajout));
		
		volume_baignoire.setText("1000");
		debit_ajout.setText("25");
		debit_fuite.setText("20");
			
	}
	
	private void setValeureBaignoire(String debit, String fuite, String volumeBaignoire) {
		try {
			int d = Integer.parseInt(debit);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
