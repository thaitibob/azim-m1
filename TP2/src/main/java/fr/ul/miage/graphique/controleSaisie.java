package fr.ul.miage.graphique;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class controleSaisie implements ChangeListener<String>{
	
	private TextField texte;
	
	public controleSaisie(TextField texte) {
		super();
		this.texte = texte;
	}

	@Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
    	if (!newValue.matches("\\d*")) {
            texte.setText(newValue.replaceAll("[^\\d]", ""));
        }
    }
}
