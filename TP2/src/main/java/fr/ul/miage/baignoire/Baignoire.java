package fr.ul.miage.baignoire;

public class Baignoire implements Runnable{
	
	public static int quantiteEau = 0;
	public static int volume_max = 1000;
	
	public static int total_fuite=0;
	public static int fuite = 0;
	
	public void fuite(int debit) {
		if(Baignoire.quantiteEau>=20) {
			Baignoire.quantiteEau -= debit;
			Baignoire.total_fuite+=debit;
		}
	}
	
	@Override
	public void run() {
		this.fuite(Baignoire.fuite);
	}
}
