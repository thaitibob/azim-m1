package fr.ul.miage.baignoire;

public class Robinet implements Runnable{
	
	public static int total_remplissage=0;
	public static int debit_entree = 0;
	
	public void debit(int debit) {
		if(Baignoire.quantiteEau<Baignoire.volume_max) {
			Baignoire.quantiteEau +=debit;
			Robinet.total_remplissage+=debit;
		}
	}
	
	@Override
	public void run() {
		this.debit(Robinet.debit_entree);
	}
}
