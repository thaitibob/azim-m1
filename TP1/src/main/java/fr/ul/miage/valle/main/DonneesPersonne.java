package fr.ul.miage.valle.main;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.joda.time.DateTime;

import fr.ul.miage.valle.donnees.DonneesParse;
import fr.ul.miage.valle.donnees.IdPersonne;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class DonneesPersonne implements Initializable{
	@FXML
	private Button affichage;
	
	@FXML
	private TableView<IdPersonne> PersonneData;
	
	@FXML
	private TableColumn<IdPersonne, Integer> id, nb_actions, nb_sessions, nb_temps;
	
	@FXML
	private TableColumn<IdPersonne, String> nb_temps_heure;
	
	@FXML
	private DatePicker date_inf, date_sup;
	
	private HashMap<Integer, IdPersonne> personne;
	private ObservableList<IdPersonne> observable;

	@FXML
	void maj_donnees(ActionEvent e) {
		
		int annee = this.date_inf.getValue().getYear();
		int mois = this.date_inf.getValue().getMonthValue();
		int jour = this.date_inf.getValue().getDayOfMonth();
		
		int annee1 = this.date_sup.getValue().getYear();
		int mois1 = this.date_sup.getValue().getMonthValue();
		int jour1 = this.date_sup.getValue().getDayOfMonth();
		
		DateTime d1 = new DateTime(annee,mois,jour,00,00);
		DateTime d2 = new DateTime(annee1,mois1,jour1,00,00);
		
		this.observable.removeAll(this.observable);
		this.observable = creationListe(d1, d2);
		PersonneData.setItems(this.observable);
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.date_inf.setValue(LocalDate.of(2016, 01, 01));
		this.date_sup.setValue(LocalDate.of(2020, 01, 01));
		
		DonneesParse d = new DonneesParse();
		d.ParseFichier();
		this.personne = d.getHash_map();
		this.observable = creationListe(new DateTime(2016,11,27,23,45), new DateTime(2019,11,28,00,15));

		id.setCellValueFactory(new PropertyValueFactory<>("id"));
		nb_actions.setCellValueFactory(new PropertyValueFactory<>("nombre_actions"));
		nb_sessions.setCellValueFactory(new PropertyValueFactory<>("nombreSessions"));
		nb_temps.setCellValueFactory(new PropertyValueFactory<>("dureeApproximative"));
		nb_temps_heure.setCellValueFactory(new PropertyValueFactory<>("dureeApproximativeHeure"));
		
		PersonneData.setItems(this.observable);
		
	}
	
	private ObservableList<IdPersonne> creationListe(DateTime d1, DateTime d2){
        ArrayList<IdPersonne> listePersonne = new ArrayList<IdPersonne>() ;
        for (Map.Entry<Integer, IdPersonne> entry : this.personne.entrySet()){
        	IdPersonne personne1 = entry.getValue();
        	//on trie l'arraylist de connexion par ordre de date croissant pour effectuer les comparaisons
		    personne1.triConnexion();
		    // on effectue les calculs
		    personne1.calculDonnees(d1,d2);
            listePersonne.add(personne1);
        }
        ObservableList<IdPersonne> liste = FXCollections.observableList(listePersonne);
        return liste;
    }
}
