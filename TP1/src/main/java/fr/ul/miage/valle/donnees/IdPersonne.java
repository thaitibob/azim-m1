package fr.ul.miage.valle.donnees;

import java.util.ArrayList;
import java.util.Collections;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class IdPersonne {
	
	private int identifiant;
	
	private ArrayList<DateTime> connexionArrayList = new ArrayList<DateTime>();
	
	private SimpleStringProperty id;
	private SimpleIntegerProperty nombre_actions;
	private SimpleIntegerProperty dureeApproximative;
	private SimpleIntegerProperty nombreSessions;
	private SimpleStringProperty dureeApproximativeHeure;
	
	
	public SimpleStringProperty idProperty(){
		return id;
	}
	
	public SimpleStringProperty dureeApproximativeHeureProperty(){
		return dureeApproximativeHeure;
	}
	
	public String getid() {
	    return id.get();
	}

	public void setId(SimpleStringProperty id) {
		this.id = id;
	}

	public SimpleIntegerProperty nombre_actionsProperty() {
		return nombre_actions;
	}

	public void setNombre_actions(SimpleIntegerProperty nombre_actions) {
		this.nombre_actions = nombre_actions;
	}

	public SimpleIntegerProperty dureeApproximativeProperty() {
		return dureeApproximative;
	}

	public void setDureeApproximative(SimpleIntegerProperty dureeApproximative) {
		this.dureeApproximative = dureeApproximative;
	}

	public SimpleIntegerProperty nombreSessionsProperty() {
		return nombreSessions;
	}

	public void setNombreSessions(SimpleIntegerProperty nombreSessions) {
		this.nombreSessions = nombreSessions;
	}

	public int getIdentifiant() {
		return identifiant;
	}

	public IdPersonne(int identifiant, DateTime connexion) {
		this.identifiant = identifiant;
		this.connexionArrayList = new ArrayList<DateTime>();
		this.connexionArrayList.add(connexion);
		
		this.id = new SimpleStringProperty(String.valueOf(identifiant));
	}
	
	public int getIdentifiantProperty() {
		return identifiant;
	}
	
	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}
	
	public ArrayList<DateTime> getConnexionArrayList() {
		return connexionArrayList;
	}
	
	public void ajoutConnexion(DateTime connexion) {
		this.connexionArrayList.add(connexion);
	}
	
	public void calculDonnees(DateTime date1, DateTime date2){
		int nombre_sessions=0;
		int nombre_actions=0;
		int duree_approximative=0;
		
		for(int i=0 ; i<this.connexionArrayList.size()-1 ; i++) {
			DateTime convert = this.connexionArrayList.get(i);
			DateTime convert_suivant = this.connexionArrayList.get(i+1);
			if( (convert.compareTo(date1)==1 || convert.compareTo(date1)==0) 
					&& (convert.compareTo(date2)==-1 || convert.compareTo(date2)==0)) {
				nombre_actions++;
				int minutes = Minutes.minutesBetween(convert, convert_suivant).getMinutes();
				if(minutes>30) {
					nombre_sessions++;
				}
				if(minutes<30) {
					duree_approximative+=minutes;
				}
			}
			if(i==this.connexionArrayList.size()-1 && 
					(convert.compareTo(date1)==1 || convert.compareTo(date1)==0) 
					&& (convert.compareTo(date2)==-1 || convert.compareTo(date2)==0)) {
				nombre_sessions++;
			}
			// si il y a qu'une session pas de comparaison donc 1
			if(nombre_actions==1) {
				nombre_sessions++;
			}
		}
		int res = duree_approximative/60;
		this.nombre_actions = new SimpleIntegerProperty(nombre_actions);
		this.dureeApproximativeHeure = new SimpleStringProperty(res+"H"+duree_approximative%60);
		this.dureeApproximative = new SimpleIntegerProperty(duree_approximative);
		this.nombreSessions = new SimpleIntegerProperty(nombre_sessions);
	}
	
	public int nb_connexion() {
		return this.connexionArrayList.size();
	}
	
	public void setConnexionArrayList(ArrayList<DateTime> connexionArrayList) {
		this.connexionArrayList = connexionArrayList;
	}
	
	public void triConnexion() {
		Collections.sort(this.connexionArrayList);
	}
				
	public static DateTime conversionDate(String date1) {
		String date 	= date1.split(";")[0];
		String temps 	= date1.split(";")[1];
		int annee 		= Integer.parseInt(date.split("/")[0]);
		int mois 		= Integer.parseInt(date.split("/")[1]);
		int jour 		= Integer.parseInt(date.split("/")[2]);
		int heure 		= Integer.parseInt(temps.split(":")[0]);
		int minutes 	= Integer.parseInt(temps.split(":")[1]);
		DateTime dateTime = new DateTime(annee,mois,jour,heure,minutes);
		
		return dateTime;
	}
	
}
