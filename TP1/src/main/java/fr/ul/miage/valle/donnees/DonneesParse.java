package fr.ul.miage.valle.donnees;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import fr.ul.miage.valle.main.TP1;

public class DonneesParse {
	
	private static final Logger LOG = Logger.getLogger(DonneesParse.class.getName());
	
	private HashMap<Integer, IdPersonne> hash_map = new HashMap<Integer, IdPersonne>();
	
	public DonneesParse() {

	}
	
	public HashMap<Integer, IdPersonne> getHash_map() {
		return hash_map;
	}

	public void setHash_map(HashMap<Integer, IdPersonne> hash_map) {
		this.hash_map = hash_map;
	}
	
	public int CompterNombreConnexion(int key) {
		IdPersonne personne = this.hash_map.get(key);
		return personne.nb_connexion();
	}

	public void ParseFichier() {
		
		DonneesParse app = new DonneesParse();
		try {
			CSVParser p = app.buildCSVParser();
			for(CSVRecord r : p) {
				try {
					int identifiant = Integer.parseInt(r.get(0));
					String connexion = r.get(1);
					
					String heureString = (connexion.split(",")[1]).replaceAll("\\s","");
					String dateString = connexion.split(",")[0];
					
					String anneeString = "20"+dateString.split(" ")[2];
					String jourString = dateString.split(" ")[0];
					String moiString = replaceMois(dateString.split(" ")[1]);
					
					connexion = anneeString+"/"+moiString+"/"+jourString+";"+heureString;

					if(this.hash_map.containsKey(identifiant)) {
						IdPersonne personne = this.hash_map.get(identifiant);
						personne.ajoutConnexion(IdPersonne.conversionDate(connexion));
						this.hash_map.put(identifiant, personne);
					}else {
						this.hash_map.put(identifiant, new IdPersonne(identifiant, IdPersonne.conversionDate(connexion)));
					}					
				} catch (Exception e) {
					LOG.severe("Donnée non valide"+e);
				}
			}
		}catch(IOException e) {
			LOG.severe("Erreur dans le lecture dans le fichier CSV");
		}
	}
	
	private String replaceMois(String mois) {
		if(mois.equals("janv.")) {
			return "01";
		}
		if(mois.equals("févr.")) {
			return "02";
		}
		if(mois.equals("mars")) {
			return "03";
		}
		if(mois.equals("avril")) {
			return "04";
		}
		if(mois.equals("mai")) {
			return "05";
		}
		if(mois.equals("juin")) {
			return "06";
		}
		if(mois.equals("juil.")) {
			return "07";
		}
		if(mois.equals("août")) {
			return "08";
		}
		if(mois.equals("sept.")) {
			return "09";
		}
		if(mois.equals("oct.")) {
			return "10";
		}
		if(mois.equals("nov.")) {
			return "11";
		}
		if(mois.equals("déc.")) {
			return "12";
		}
		return null;
	}
	
	public CSVParser buildCSVParser() throws IOException{
		CSVParser res = null;
		Reader in;
		in = new FileReader(TP1.fileString);
		CSVFormat csvf = CSVFormat.DEFAULT.withCommentMarker('#').withDelimiter(';').withFirstRecordAsHeader();
		res = new CSVParser(in, csvf);
		return res;
	}
	
	
}
