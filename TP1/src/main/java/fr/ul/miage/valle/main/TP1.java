package fr.ul.miage.valle.main;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TP1 extends Application {
	
	private static final Logger LOG = Logger.getLogger(TP1.class.getName());
	public static String fileString;

	@Override
	public void start(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("application.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static void main(String[] args) {
		
		String filename=null;
		Options options = new Options();
		Option input = new Option ("i", "input", true,"nom du fichier csv contenant la liste des données");
		input.setRequired(true);
		options.addOption(input);
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine line = parser.parse(options, args);
			if(line.hasOption("i")) {
				filename = line.getOptionValue("i");
			}
		}
		catch(ParseException exp){
			LOG.severe("Erreur dans la ligne de commande");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("TP1",options);
			System.exit(1);
		}
		
		TP1.fileString = filename;
		launch(args);
	}
	
}
