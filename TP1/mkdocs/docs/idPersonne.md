# Classe IdPersonne

## Package

Cette classe se trouve dans le package : `fr.ul.miage.valle.donnees`

## Attributs de classe

* Un identifiant qui correspond à la personne
``` java
private int identifiant;
```

* Un arrayList de dates, il enregistre la liste des connexions pour la personne
``` java
private ArrayList<DateTime> connexionArrayList = new ArrayList<DateTime>();
```

Les autres attributs sont explicites et permettent de délivrer les informations nécessaires à la réalisation du TP.

## Fonction calculDonnees

La fonction prend en paramètre deux dates, permettant d'avoir l'interval souhaité. Elle ne retourne rien.

``` java
public int calculDonnees(DateTime date1, DateTime date2){
	...

}
```

Un parcours de l'arrayList de connexion permet de comparer la date 'i' avec les dates passées en paramètres. Les compteurs de sessions, temps et nombre connexion sont mis à jour en conséquence. A la fin de la fonction, les attributs prennent ainsi les valeures souhaités et seront accessibles par getter.

``` java
for(int i=0 ; i<this.connexionArrayList.size()-1 ; i++) {
	...
	if( (convert.compareTo(date1)==1 || convert.compareTo(date1)==0) 
			&& (convert.compareTo(date2)==-1 || convert.compareTo(date2)==0)) {
	...
}
...
this.nombre_actions = new SimpleIntegerProperty(nombre_actions);
this.dureeApproximativeHeure = new SimpleStringProperty(res+"H"+duree_approximative%60);
this.dureeApproximative = new SimpleIntegerProperty(duree_approximative);
this.nombreSessions = new SimpleIntegerProperty(nombre_sessions);
```


## Fonction conversionDate

Elle prend en paramètre une chaîne de caractère de forme : "2019/01/15;15:30". Elle convertie cette châine en type Datetime (librairie joda-time) permettant la comparaison des connexions.

``` java
public static DateTime conversionDate(String date1) {
	...
	DateTime dateTime = new DateTime(annee,mois,jour,heure,minutes);
	return dateTime;
}
```
