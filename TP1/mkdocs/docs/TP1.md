# Classe TP1

## Package

Cette classe se trouve dans le package : `fr.ul.miage.valle.main`

## Attributs de classe

Il y a donc deux attributs de classe statiques.

* Un logger
``` java
private static final Logger LOG = Logger.getLogger(TP1.class.getName());
```

* Un paramètre qui récupère le chemin du fichier à analyser
``` java
private static String fileString;
```

## Fonction start

Début de notre application javaFX. On initialise la vue.

``` java
@Override
	public void start(Stage primaryStage) {
		...
	}
```

## Fonction Main

Début de notre programme. Prend en arguement les paramètres passés. On analyse le chemin passé en argument du programme pour tester la validité. S'il est valide le chemin du fichier est alors passé dans l'attribut static de la classe.

``` java
public static void main(String[] args) {
		String filename=null;
		...
		try {
			CommandLine line = parser.parse(options, args);
			if(line.hasOption("i")) {
				filename = line.getOptionValue("i");
			}
		}
		...
		TP1.fileString = filename;
		launch(args);
	}
```
