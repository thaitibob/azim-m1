# Classe DonneesParse

## Package

Cette classe se trouve dans le package : `fr.ul.miage.valle.donnees`

## Attributs de classe

* Un logger
``` java
private static final Logger LOG = Logger.getLogger(DonneesParse.class.getName());
```

* Hashmap de Personne en fonction de leur identifiant
``` java
private HashMap<Integer, IdPersonne> hash_map = new HashMap<Integer, IdPersonne>();
```

## Fonction ParseFichier

Fonction de parsage du fichier rentré. Elle formate les dates au format châine de caractère "AAAA/MM/JJ;HH:mm" et crée rempli la hashMap en fonction des identifiant et des liste de connexions par identifiant.

``` java
public void ParseFichier() {
	...
	IdPersonne personne = this.hash_map.get(identifiant);
	personne.ajoutConnexion(IdPersonne.conversionDate(connexion));
	this.hash_map.put(identifiant, personne);
	...
}
```

## Fonction replaceMois

La fonction formate juste le fichier et transforme les mois en nombre pour être utilisé par la librairie DateTime.
```
Exemple : févr. devient ainsi => "02"
```

## Fonction buildCSVParser

Parse le fichier avec un reader. La fonction retourne un CSVParser. On appele la méthode static de notre classe principale pour récupérer la location du fichier.
``` java
public CSVParser buildCSVParser() throws IOException{
	...
	in = new FileReader(TP1.fileString);
	...
}
```
