# Classe DonneesPersonne

## Package

Cette classe se trouve dans le package : `fr.ul.miage.valle.main`

## Attributs de classe

* La liste des attributs @FXML sont explicites et composent l'interface

* Une hashmap qui stock pour un identifiant donné, un objet IdPersonne qui regroupe toutes les connexions
``` java
private HashMap<Integer, IdPersonne> personne;
```

* Une observableList qui regroupe les résultats souhaités et mis à jour lorsque les intervalles de dates changent.
``` java
private ObservableList<IdPersonne> observable;
```

## Fonction initialize

Fonction d'initialisation de l'interface. Un thread permet la création du controller et le passage en paramètre du fichier avant initialisation de l'interface. Dans ce thread, il y a initialisation de l'interface.

``` java
@Override
public void initialize(URL location, ResourceBundle resources) {
	Platform.runLater(() -> {
		...
		id.setCellValueFactory(new PropertyValueFactory<>("id"));
		nb_actions.setCellValueFactory(new PropertyValueFactory<>("nombre_actions"));
		nb_sessions.setCellValueFactory(new PropertyValueFactory<>("nombreSessions"));
		nb_temps.setCellValueFactory(new PropertyValueFactory<>("dureeApproximative"));
		nb_temps_heure.setCellValueFactory(new PropertyValueFactory<>("dureeApproximativeHeure"));
		PersonneData.setItems(this.observable);
    });
}
```

## Fonction maj_donnees

La fonction récupère l'intervalle de dates souhaités, met à jour l'observableList et les données dans le tableau.

``` java
@FXML
void maj_donnees(ActionEvent e) {
	int annee = this.date_inf.getValue().getYear();
	...
	this.observable.removeAll(this.observable);
	this.observable = creationListe(d1, d2);
	PersonneData.setItems(this.observable);
}
```

## Fonction creationListe

La fonction permet de créer une observableList. Elle prend en paramètre l'interval de date souhaité et calcul pour chaque personne les données souhaités. La fonction retourne une observaleList.

``` java
private ObservableList<IdPersonne> creationListe(DateTime d1, DateTime d2){
    ArrayList<IdPersonne> listePersonne = new ArrayList<IdPersonne>() ;
    for (Map.Entry<Integer, IdPersonne> entry : this.personne.entrySet()){
    		...
	  	personne1.calculDonnees(d1,d2);
        	...
    }
    ObservableList<IdPersonne> liste = FXCollections.observableList(listePersonne);
    return liste;
}
```
