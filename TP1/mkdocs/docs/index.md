# TP1

##But du projet

Ce projet a pour but de nous apprendre à utiliser plusieurs tehcnologies.

* Maven
* JavaFX
* Utilisation de la Mkdocs

##Description du projet

A l'aide d'un fichier CSV fournit regroupant des dates de connexions par identifiant, nous devons par interface graphique pouvoir retrouver pour une période donnée :

* Le nombre de connexions
* La durée approximative de connexion
* Le nombre de sessions

## Commande d'utilisation

Il faut se placer dans ~/TP1/bindist/bin.   
Attention : le fichier CSV a été reformaté en UTF-8 pour le TP, si vous utilisez un autre CSV, la lecture ne pourra pas fonctionner.

* `./TP1 -i  ~/data.csv` - Execute le projet pour le fichier data.csv.

##Dépendances

Plusieurs dépendances ont été utilisé pour réaliser ce projet

* Apache Commons CLI
* Apache Commons CSV
* joda-time (gestionnaire de dates)

## Auteur

* Etudiant : Jonathan Valle - M1 MIAGE
* Professeur : M. Azim Roussalany
* Université de Lorraine 2019-2020

## Disposition du projet

    TP1/
        src/main/java/  						# Classes de l'application
        		fr.ul.miage.valle.donnees
        			DonneesParse.java			# Clsse de parsage du fichier CSV
        			IdPersonne.java				# Classe regroupant les attributs d'une personne	
        		fr.ul.miage.valle.main
        			DonneesPersonne.java		# Actions de l'interface grpahique
        			TP1.java					# Classe principale
        			application.fxml			# xml de configuration de l'interface graphique
        mkdocs/  								# Documentation de l'application
        		site/
        			index.html					#lancer la mkdocs
        samples/
        		data.csv						#jeu de données à analyser
